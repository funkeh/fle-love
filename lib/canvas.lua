function remakeCanvases(gameWidth, gameHeight)

    -- Recreate the canvases
    itemCanvas = lg.newCanvas(256, gameHeight - 24)
    viewCanvas = lg.newCanvas(gameWidth - itemCanvas:getWidth(), gameHeight - 24)


    lg.setCanvas(viewCanvas)

    lg.setColor(90, 200, 100)
    lg.rectangle('fill', 0, 0, viewCanvas:getWidth(), viewCanvas:getHeight())

    lg.setCanvas(itemCanvas)

    lg.setColor(200, 100, 90)
    lg.rectangle('fill', 0, 0, itemCanvas:getWidth(), itemCanvas:getHeight())

    lg.setCanvas()

end



function drawCanvases()

    lg.setColor(255, 255, 255)

    lg.draw(viewCanvas, 0, 24)
    lg.draw(itemCanvas, viewCanvas:getWidth(), 24)

end
