-- chikun :: 2015
-- Maths functions



--
function math.clamp(min, val, max)

    val = math.max(min, val)
    val = math.min(max, val)

    return val

end



-- Returns the distance between two points.
function math.dist(x1,y1, x2,y2)

    return ((x2-x1)^2+(y2-y1)^2)^0.5

end
