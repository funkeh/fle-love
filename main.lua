-- chikun :: 2015
-- funkeh's Level Editor for LÖVE


require "lib/canvas"            -- Functions for resetting canvases
require "lib/shorthand"         -- Shorthand for libraries


-- Performed on game startup
function love.load()

    -- Reset all canvases
    remakeCanvases(lw.getWidth(), lw.getHeight())

end



-- Performed on game update
function love.update(dt)

    -- placeholder

end



-- Performed on game draw
function love.draw()

    -- Reset colour
    love.graphics.setColor(255, 255, 255)

    -- Draw canvases
    drawCanvases()

end



-- Performed on window resize
function love.resize(width, height)

    -- Resize canvases
    remakeCanvases(width, height)

end
