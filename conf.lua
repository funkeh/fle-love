-- chikun :: 2015
-- Configuration file

function love.conf(game)


    game.window.width   = 1280
    game.window.height  = 660
    game.window.minwidth    = 640
    game.window.minheight   = 360

    game.window.title   = "fLE"
    game.window.resizable = true


end
